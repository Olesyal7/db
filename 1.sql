CREATE TABLE COMPANY (
  company_id SERIAL PRIMARY KEY ,
  company_nm VARCHAR(30) ,
  established_dttm TIMESTAMP(0) ,
  site_url VARCHAR(100) ,
  office_adress_txt VARCHAR(50)
);


CREATE TABLE TENANT (
  tenant_id SERIAL PRIMARY KEY ,
  first_nm VARCHAR(30) ,
  last_nm VARCHAR(30) ,
  middle_nm VARCHAR(30) ,
  adress_txt VARCHAR(50) ,
  rent_rate INTEGER CHECK(rent_rate >0) ,
  status_code VARCHAR(30)
);


CREATE TABLE QUEST_ROOM (
  quest_room_id SERIAL PRIMARY KEY ,
  adress_txt VARCHAR(40) ,
  quest_room_nm VARCHAR(30) ,
  type_code VARCHAR(20) ,
  cost_amt INTEGER ,
  company_id INTEGER REFERENCES COMPANY(company_id) ,
  tenant_id INTEGER REFERENCES TENANT(tenant_id)
);


CREATE TABLE PARTICIPANT (
  participant_id SERIAL PRIMARY KEY ,
  first_nm VARCHAR(30) ,
  last_nm VARCHAR(30) ,
  middle_nm VARCHAR(30) ,
  age_amt INTEGER CHECK(age_amt > 11 AND age_amt < 111) ,
  phone_no VARCHAR(10) ,
  wage_rate INTEGER CHECK(wage_rate > 0) ,
  quality_code VARCHAR(20) ,
  type_code VARCHAR(15)
);


CREATE  TABLE APPLICATION (
  application_id SERIAL PRIMARY KEY ,
  application_dttm TIMESTAMP(0) ,
  people_cnt INTEGER CHECK(people_cnt > 0) ,
  pay_type_code VARCHAR(15) ,
  quest_room_id INTEGER REFERENCES QUEST_ROOM(quest_room_id)
);


CREATE TABLE ADVERTISING (
  advertising_id SERIAL PRIMARY KEY ,
  type_code VARCHAR(20) ,
  advertising_nm VARCHAR(30) ,
  circulation_amt INTEGER CHECK(circulation_amt > 0)
);


CREATE TABLE PARTICIPANT_X_QUEST_ROOM (
  participant_id SERIAL REFERENCES PARTICIPANT(participant_id) ,
  quest_room_id SERIAL REFERENCES QUEST_ROOM(quest_room_id)
);

CREATE TABLE PARTICIPANT_X_COMPANY (
  participant_id SERIAL REFERENCES PARTICIPANT(participant_id) ,
  company_id SERIAL REFERENCES COMPANY(company_id)
);

CREATE TABLE APPLICATION_X_PARTICIPANT (
  application_id SERIAL REFERENCES APPLICATION(application_id) ,
  participant_id SERIAL REFERENCES PARTICIPANT(participant_id)
);

CREATE TABLE ADVERTISING_X_COMPANY (
  advertising_id SERIAL REFERENCES ADVERTISING(advertising_id),
  company_id     SERIAL REFERENCES COMPANY(company_id)
);

DROP TABLE PARTICIPANT_X_QUEST_ROOM;
DROP TABLE PARTICIPANT_X_COMPANY;
DROP TABLE APPLICATION_X_PARTICIPANT;
DROP TABLE ADVERTISING_X_COMPANY;
DROP TABLE ADVERTISING;
DROP TABLE APPLICATION;
DROP TABLE PARTICIPANT;
DROP TABLE QUEST_ROOM;
DROP TABLE COMPANY;
DROP TABLE TENANT;

