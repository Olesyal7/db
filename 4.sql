CREATE TABLE COMPANY (
  company_id SERIAL PRIMARY KEY ,
  company_nm VARCHAR(30) ,
  established_dttm TIMESTAMP(0) ,
  site_url VARCHAR(100) ,
  office_adress_txt VARCHAR(50)
);

INSERT INTO COMPANY(company_nm, established_dttm, site_url, office_adress_txt) (
VALUES ('Квест-квест-квест', '2014-02-15', 'https://3_quest.com', 'Первомайская 32, к.2')
    , ('Лучшие квесты', '2017-06-13', 'https://the_best_quest.com', 'Второомайская 16, к.1')
    , ('Страшные квесы', '2016-06-06', 'https://horror_quest.com', 'Третьемайская 47')
    , ('Интеллектуальные квесты', '2016-04-09', 'https://clever_quest.com', 'Улица Пушкина 10')
    , ('Шерлок Холмс', '2013-07-08', 'https://Holms_quest.com', 'Новый Арбат 12')
    , ('Сеть квестов имени Андрея Б', '2018-11-18', 'https://AB_quest.com', '1-й московский тупик 13')
    , ('Выйди из комнаты', '2017-05-18', 'https://out_room_quest.com', 'Космонавтов 18')
);

UPDATE COMPANY
SET company_nm = 'The best'
WHERE company_nm = 'Лучшие квесты';

DELETE FROM COMPANY
WHERE established_dttm < '2017-01-01';



CREATE TABLE PARTICIPANT (
  participant_id SERIAL PRIMARY KEY ,
  first_nm VARCHAR(30) ,
  last_nm VARCHAR(30) ,
  middle_nm VARCHAR(30) ,
  age_amt INTEGER CHECK(age_amt > 11 AND age_amt < 111) ,
  phone_no VARCHAR(10) ,
  wage_rate INTEGER CHECK(wage_rate > 0) ,
  quality_code VARCHAR(20) ,
  type_code VARCHAR(15)
);

INSERT INTO PARTICIPANT(first_nm, last_nm, middle_nm, age_amt, phone_no, wage_rate, quality_code, type_code) (
VALUES ('Иван', 'Иванов', 'Иванович', 40, 9105888888, NULL, NULL, 'Клиент')
  , ('Петр', 'Петров', 'Петрович', 18, 9158856884, NULL, NULL, 'Клиент')
  , ('Ирина', 'Кузнецова', 'Дмитриевна', 26, 9207788885, NULL, NULL, 'Клиент')
  , ('Петр', 'Иванов', 'Петрович', 38, 9208855586, NULL, NULL, 'Клиент')
  , ('Никита', 'Травин', 'Константинович', 18, 9158485976, NULL, NULL, 'Клиент')
  , ('Валерий', 'Лысков', 'Николаевич', 26, 9244879878, NULL, NULL, 'Клиент')
  , ('Александр', 'Лобачев', 'Александрович', 38, 9205748595, NULL, NULL, 'Клиент')
  , ('Мария', 'Прохорова', 'Петровна', 18, 9158467936, NULL, NULL, 'Клиент')
  , ('Елена', 'Кузнецова', 'Дмитриевна', 26, 9207322222, NULL, NULL, 'Клиент')
  , ('Александра', 'Гордеева', 'Анатольевна', 38, 9204890923, NULL, NULL, 'Клиент')
  , ('Сидр', 'Сидоров', 'Сидорович', 22, 9207865387, 500, 'Профессионал', 'Актер')
  , ('Иван', 'Петров', 'Иванович', 40, 9107656765, 700, 'Со стажем', 'Актер')
  , ('Мария', 'Петрова', 'Константиновна', 20, 9108191583, 200, 'Новичек', 'Актер')
  , ('Николай', 'Федотов', 'Афонасьевич', 34, 9066360842, 4000, 'Народный артист', 'Актер')
  , ('Мария', 'Житкова', 'Павловна', 22, 9207865387, NULL, NULL, 'Владелец')
  , ('Степан', 'Петров', 'Иванович', 40, 9107656765, NULL, NULL, 'Владелец')
  , ('Иван', 'Петрова', 'Константиновна', 20, 9108493087, NULL, NULL, 'Владелец')
  , ('Евгений', 'Федотов', 'Афонасьевич', 34, 9066778908, NULL, NULL, 'Владелец')
);

UPDATE PARTICIPANT
SET wage_rate = wage_rate + 100
WHERE type_code = 'Актер';

DELETE FROM PARTICIPANT
WHERE age_amt < 20;