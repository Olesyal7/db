--1)Вывести все типы квест-комнат

SELECT DISTINCT type_code
FROM QUEST_ROOM;

--2)Посчитать число совершеннолетних посетителей

SELECT COUNT(participant_id)
FROM PARTICIPANT
WHERE age_amt > 17
      AND type_code = 'Клиент';

--3)Вывести все квет-комнаты, в которые приходят люди в количестве 4-6 человек

SELECT DISTINCT quest_room_nm
FROM QUEST_ROOM
INNER JOIN APPLICATION
  ON QUEST_ROOM.quest_room_id = APPLICATION.quest_room_id
WHERE people_cnt >= 4 AND people_cnt <= 6;

--4)Найти компании, в которых более одной квест-комнаты, и все они одного типа

SELECT company_nm, MIN(type_code)
FROM COMPANY
  INNER JOIN QUEST_ROOM
  ON QUEST_ROOM.company_id = COMPANY.company_id
GROUP BY COMPANY.company_id
HAVING COUNT(DISTINCT type_code) = 1 AND COUNT(quest_room_id) > 1;

--1)Вывести компании, которые проводили квесты еще до того, как это стало мейнстримом

SELECT company_nm, MIN(application_dttm)
FROM COMPANY
INNER JOIN QUEST_ROOM
  ON QUEST_ROOM.quest_room_id = COMPANY.company_id
INNER JOIN APPLICATION
  ON APPLICATION.quest_room_id = QUEST_ROOM.quest_room_id
GROUP BY company.company_id
HAVING MIN(application_dttm) < '2018-01-01';
