CREATE OR REPLACE VIEW avg_circ AS (
  SELECT DISTINCT
    MAX(type_code)
    , AVG(circulation_amt)
    FROM advertising
      GROUP BY type_code
);

CREATE OR REPLACE VIEW quest_appl AS (
  SELECT
    application.people_cnt
    , quest_room_nm
    , application_dttm
  FROM
    application
  INNER JOIN
    quest_room
  ON
    application.quest_room_id = quest_room.quest_room_id
);

CREATE OR REPLACE VIEW date_cnt AS (
  SELECT
    SUM(people_cnt) AS sum_p
    , MAX(application_dttm::DATE)
  FROM
    application
  GROUP BY
    application_dttm::date
);

CREATE OR REPLACE VIEW quest_price AS (
  SELECT
    AVG(cost_amt)
  FROM
    quest_room
);

CREATE OR REPLACE VIEW web_company AS (
  SELECT
    company_nm
    , site_url
  FROM company
    WHERE site_url IS NOT NULL
);

CREATE OR REPLACE VIEW owner AS (
  SELECT
    first_nm
    , last_nm
    , (CASE WHEN
          phone_no IS NULL THEN 'Не указан'
            ELSE
          phone_no END) AS phone_no
    , company_nm
  FROM
    participant_x_company
  INNER JOIN
    participant
  ON
    participant.participant_id = participant_x_company.participant_id
  INNER JOIN
    company
  ON
    participant_x_company.company_id = company.company_id
  WHERE type_code = 'Владелец'
);

CREATE OR REPLACE VIEW client_18 AS (
  SELECT
    first_nm
    , last_nm
    , age_amt
  FROM
    participant
  WHERE age_amt >= 18
);

CREATE OR REPLACE VIEW tenant_best AS (
  SELECT
    first_nm
    , last_nm
    , adress_txt
  FROM
    tenant
  WHERE status_code = 'Отличное'
);
